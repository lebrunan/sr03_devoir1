package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class ClientConnection extends Thread {

    private final List<ClientConnection> clientConnections;
    private String name;
    private final DataInputStream dataInputStream;
    private final DataOutputStream dataOutputStream;
    private boolean isConnected;

    public ClientConnection(List<ClientConnection> clientConnections, Socket socket) throws IOException {
        this.clientConnections = clientConnections;
        this.dataInputStream = new DataInputStream(socket.getInputStream());
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
        this.isConnected = false;
    }

    @Override
    public void run() {
        try {
            handleName();
            announceConnection();
            handleMessages();

        } catch (Exception e) {
            System.out.println("Exception with client : " + name);
            e.printStackTrace();

        } finally {
            clientConnections.remove(this);
            this.interrupt();
        }
    }

    private void announceConnection() throws IOException {
        spreadMessage(name + " entered the room");
    }

    /**
     * This method receive message from current client and spread his message to
     * other users. It also handles disconnection. 
     */
    private void handleMessages() throws IOException {
        String message;
        try {
            do {
                message = receiveMessageFromClient();
                spreadMessage(name + " : " + message);
            } while (!message.equals("exit")) ;
        } catch(IOException ignored){}
        spreadMessage(name + " left the room");
    }

    private void spreadMessage(String message) throws IOException {
        for (ClientConnection clientConnection : clientConnections) {

            //Send message to everybody except sender, and only if user has set his name.
            if ( ! isConnectionCurrentConnection(clientConnection) && hasClientSetHisName(clientConnection)) {
                clientConnection.dataOutputStream.writeUTF(message);
            }
        }
    }

    private boolean hasClientSetHisName(ClientConnection clientConnection) {
        return clientConnection.isConnected;
    }

    private boolean isConnectionCurrentConnection(ClientConnection clientConnection) {
        return clientConnection.equals(this);
    }

    private String receiveMessageFromClient() throws IOException {
        return dataInputStream.readUTF();
    }

    private void handleName() throws IOException {
        // Part 1 : read client name
        String name = dataInputStream.readUTF();

        // Part 2 : check if this name is already bind
        while (isNameAlreadyTaken(name)) {


            //  Part 3 : If so, ask another time for user name and send "false" to client
            dataOutputStream.writeBoolean(false);
            name = dataInputStream.readUTF();

        }


        // Part 4 : This name is unique => send "true" to client
        dataOutputStream.writeBoolean(true);
        this.name = name;
        this.isConnected = true;
    }

    private boolean isNameAlreadyTaken(String name) {
        return clientConnections.stream()
                .anyMatch(clientConnection -> clientConnection.name != null
                        && clientConnection.name.equals(name));
    }
}
