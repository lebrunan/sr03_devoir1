package server;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        List<ClientConnection> clientConnections = Collections.synchronizedList(new ArrayList<>());
        ServerSocket serverSocket = new ServerSocket(10080);

        while (true) {
            Socket socket = acceptClient(serverSocket);

            ClientConnection clientConnexion = new ClientConnection(clientConnections, socket);
            clientConnections.add(clientConnexion);

            clientConnexion.start();
        }
    }

    private static Socket acceptClient(ServerSocket serverSocket) throws IOException {
        System.out.println("Waiting for client ...");
        Socket accept = serverSocket.accept();
        System.out.println("Client connected");

        return accept;
    }

}

