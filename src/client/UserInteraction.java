package client;

import java.util.Scanner;

public class UserInteraction {
    public static Scanner scanner = new Scanner(System.in);

    public static String getString() {
        return scanner.nextLine();
    }
}
