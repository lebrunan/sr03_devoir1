package client;

import java.io.DataOutputStream;
import java.io.IOException;

public class MessageSender extends Thread {

    private final DataOutputStream dataOutputStream;

    public MessageSender(DataOutputStream dataOutputStream) {
        this.dataOutputStream = dataOutputStream;
    }

    @Override
    public void run() {

        System.out.println("You can now send messages, please pay attention to your language");

        String message = "";
        while (true) {
            message = UserInteraction.getString();
            if (message.equals("exit")) {
                break;
            }
            try {
                sendString(message);
            } catch (IOException e) {
                System.out.println("Connection reset by peer. Cannot send message.");
                break;
            }
        }
        this.interrupt();
    }

    public void sendString(String name) throws IOException {
        dataOutputStream.writeUTF(name);
    }
}
