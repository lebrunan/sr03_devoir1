package client;

import java.io.DataInputStream;
import java.io.IOException;

public class MessageReceptor extends Thread {

    private final DataInputStream dataInputStream;

    public MessageReceptor(DataInputStream dataInputStream) {
        this.dataInputStream = dataInputStream;
    }

    @Override
    public void run() {
        while (true){
            try {
                System.out.println(dataInputStream.readUTF());
            } catch (IOException e) {
                break;
            }
        }
        this.interrupt();
    }

    public boolean doesServerAcceptThisName() throws IOException {
        return ! dataInputStream.readBoolean();
    }
}
