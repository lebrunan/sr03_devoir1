package client;

import java.io.*;
import java.net.Socket;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        try {
            Socket socket = connectToServer();
            System.out.println("You are connected to chat.");

            //Open output and input stream to exchange with server
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

            // Create thread which manage message sending
            MessageSender messageSender = new MessageSender(dataOutputStream);
            // Create thread which manage message receiving
            MessageReceptor messageReceptor = new MessageReceptor(dataInputStream);

            handleName(messageSender, messageReceptor);

            // Start threads once name chosen
            messageSender.start();
            messageReceptor.start();

            // The only thread which knows if connexion should stop is MessageSender (because of "exit" message)
            synchronized (messageSender) {
                messageSender.wait();
            }

            // Close all streams and threads.
            dataInputStream.close();
            dataOutputStream.close();
            messageReceptor.interrupt();
            socket.close();
            UserInteraction.scanner.close();

        } catch (IOException e) {
            System.out.println("Server not responding... Try later.");
        } finally {
            System.exit(0);
        }

    }

    /**
     * Make user chose a nome accepted by server, this name is not stored
     */
    private static void handleName(MessageSender messageSender, MessageReceptor messageReceptor) throws IOException {
        // Send name to server
        System.out.println("Please enter your name");
        messageSender.sendString(UserInteraction.getString());

        //Ask to enter a new name as long as it is not used by another user

        while (messageReceptor.doesServerAcceptThisName()) {
            System.out.println("Your name is not unique, please enter another one");
            messageSender.sendString(UserInteraction.getString());
        }

    }

    private static Socket connectToServer() throws IOException {
        System.out.println("Connecting to the server ...");
        return new Socket("localhost", 10080);
    }

}
