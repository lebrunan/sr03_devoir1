### INSTALLATION

- First of all, clone project.

``` git clone https://gitlab.utc.fr/lebrunan/sr03_devoir1.git ```

- Then open project in your IDE.

### RUN

- Allow parallel run of client.Main

- Run server.Main

- Run one instance of client.Main

- Run another instance of client.Main